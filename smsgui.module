<?php

/**
 * Implementation of hook_gateway_info().
 */
function smsgui_gateway_info() {
  return array(
    'smsgui' => array(
      'name' => 'SMSgui',
      'configure form' => 'smsgui_admin_form',
      'send' => 'smsgui_send',
      'send form' => 'smsgui_send_form',
    ),
  );
}

/**
 * Configuration form.
 */
function smsgui_admin_form($configuration) {
  $form['smsgui_user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#description' => t('The username of your smsgui account.'),
    '#size' => 40,
    '#maxlength' => 50,
    '#default_value' => $configuration['smsgui_user'],
  );
  $form['smsgui_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('The current password of your smsgui account.'),
    '#size' => 40,
    '#maxlength' => 50,
    '#default_value' => $configuration['smsgui_password'],
  );
  $form['smsgui_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('You API key from the profile page.'),
    '#size' => 40,
    '#maxlength' => 10,
    '#default_value' => $configuration['smsgui_apikey'],
  );

  return $form;
}

/**
 * Returns custom additions to be added to the send forms.
 */
function smsgui_send_form() {
  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#description' => t('The number above will be prefixed with the selected country code'),
    '#multiple' => FALSE,
    '#options' => smsgui_country_codes(),
    '#default_value' => '',
  );
  $form['from'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#description' => t('Your name or number (max 11 characters)'),
    '#size' => 15,
    '#maxlength' => 11,
    '#default_value' => '',
  );
  
  return $form;
}

/**
 * Callback for sending messages.
 */
function smsgui_send($number, $message, $options) {
  $gateway = sms_gateways('gateway', 'smsgui');
  $config = $gateway['configuration'];

  $number = $options['country'] . $number;

  $query = 'userid=' . $config['smsgui_user'] .
           '&md5password=' . md5($config['smsgui_password']) . 
           '&apikey=' . $config['smsgui_apikey'] .
           '&to=' . $number .
           '&from=' . $options['from'] .
            // The drupal_urlencode() function introduces an unwanted '%2F'.
           '&message=' . urlencode($message);

  // Run the command.
  $http_result = drupal_http_request('http://www.smsgui.com/send.php?'. $query);

  // Check for HTTP errors.
  if ($http_result->error) {
    return array('status' => FALSE, 'message' => t('An error occured during the HTTP request: @error', array('@error' => $http_result->error)));
  }
  
  if ($http_result->data) {
    $contents = $http_result->data;
    // Read just the very first character from the result.
    $code = substr($contents, 0, 1);
    switch ($code){
      case '0': 
        $result = array('status' => FALSE, 'message' => $contents); 
        break;

      case '1':
      case '2':
        // Message accepted with warnings; assume OK.
        $result = array('status' => TRUE); 
        break;

      default: 
        // Read failed, or unrecognized code.
        $result = array('status' => FALSE, 'message' => $contents);
    }
  }
  return $result;
}

/**
 * Provide a list of country codes.
 */
function smsgui_country_codes() {
  return array(
    '' => 'No country prefix',
    '93' => 'Afghanistan (93)',
    '355' => 'Albania (355)',
    '213' => 'Algeria (213)',
    '684' => 'American Samoa (684)',
    '376' => 'Andorra (376)',
    '244' => 'Angola (244)',
    '1264' => 'Anguilla (1264)',
    '1268' => 'Antigua and Barbuda (1268)',
    '54' => 'Argentina (54)',
    '374' => 'Armenia (374)',
    '297' => 'Aruba (297)',
    '61' => 'Australia (61)',
    '43' => 'Austria (43)',
    '994' => 'Azerbaijan (994)',
    '973' => 'Bahrain (973)',
    '880' => 'Bangladesh (880)',
    '1246' => 'Barbados (1246)',
    '375' => 'Belarus (375)',
    '32' => 'Belgium (32)',
    '501' => 'Belize (501)',
    '229' => 'Benin (229)',
    '1441' => 'Bermuda (1441)',
    '975' => 'Bhutan (975)',
    '591' => 'Bolivia (591)',
    '387' => 'Bosnia Herzegovina (387)',
    '267' => 'Botswana (267)',
    '55' => 'Brazil (55)',
    '673' => 'Brunei (673)',
    '359' => 'Bulgaria (359)',
    '226' => 'Burkina Faso (226)',
    '257' => 'Burundi (257)',
    '855' => 'Cambodia (855)',
    '237' => 'Cameroon (237)',
    '1' => 'Canada (1)',
    '238' => 'Cape Verde (238)',
    '1345' => 'Cayman Islands (1345)',
    '236' => 'Central African Republic (236)',
    '235' => 'Chad (235)',
    '56' => 'Chili (56)',
    '86' => 'China (86)',
    '242' => 'Congo (242)',
    '242' => 'Congo, Democratic Republic of (242)',
    '506' => 'Costa Rica (506)',
    '385' => 'Croatia (385)',
    '53' => 'Cuba (53)',
    '357' => 'Cyprus (357)',
    '420' => 'Czech Republic (420)',
    '45' => 'Denmark (45)',
    '1809' => 'Dominican Republic (1809)',
    '20' => 'Egypt (20)',
    '503' => 'El Salvador (503)',
    '240' => 'Equatorial Guinea (240)',
    '372' => 'Estonia (372)',
    '251' => 'Ethiopia (251)',
    '298' => 'Faroe Islands (298)',
    '679' => 'Fiji (679)',
    '358' => 'Finland (358)',
    '33' => 'France (33)',
    '689' => 'French Polynesia (689)',
    '689' => 'French West Indies (689)',
    '241' => 'Gabon (241)',
    '220' => 'Gambia (220)',
    '995' => 'Georgia (995)',
    '49' => 'Germany (49)',
    '233' => 'Ghana (233)',
    '350' => 'Gibraltar (350)',
    '30' => 'Greece (30)',
    '299' => 'Greenland (299)',
    '1473' => 'Grenada (1473)',
    '1671' => 'Guam (USA) (1671)',
    '44' => 'Guernsey (44)',
    '224' => 'Guinea (224)',
    '852' => 'Hong Kong (852)',
    '36' => 'Hungary (36)',
    '354' => 'Iceland (354)',
    '91' => 'India (91)',
    '62' => 'Indonesia (62)',
    '98' => 'Iran (98)',
    '964' => 'Iraq (964)',
    '353' => 'Ireland (353)',
    '972' => 'Israel (972)',
    '39' => 'Italy (39)',
    '225' => 'Ivory Coast (225)',
    '1876' => 'Jamaica (1876)',
    '81' => 'Japan (81)',
    '44' => 'Jersey (44)',
    '962' => 'Jordan (962)',
    '7' => 'Kazakhstan (7)',
    '254' => 'Kenya (254)',
    '965' => 'Kuweit (965)',
    '996' => 'Kyrgyzstan (996)',
    '856' => 'Laos (856)',
    '371' => 'Latvia (371)',
    '961' => 'Lebanon (961)',
    '266' => 'Lesotho (266)',
    '231' => 'Liberia (231)',
    '218' => 'Libya (218)',
    '423' => 'Liechtenstein (423)',
    '370' => 'Lithuania (370)',
    '352' => 'Luxembourg (352)',
    '853' => 'Macau (853)',
    '389' => 'Macedonia (389)',
    '261' => 'Madagascar (261)',
    '262' => 'Madagascar (262)',
    '265' => 'Malawi (265)',
    '60' => 'Malaysia (60)',
    '960' => 'Maldives (960)',
    '223' => 'Mali (223)',
    '356' => 'Malta (356)',
    '44' => 'Man (Isle of) (44)',
    '222' => 'Mauritania (222)',
    '230' => 'Mauritius (230)',
    '52' => 'Mexico (52)',
    '691' => 'Micronesia, the Federated States of (691)',
    '373' => 'Moldova (373)',
    '377' => 'Monaco (377)',
    '976' => 'Mongolia (976)',
    '1664' => 'Montserrat (1664)',
    '212' => 'Morocco (212)',
    '258' => 'Mozambique (258)',
    '95' => 'Myanmar (95)',
    '264' => 'Namibi (264)',
    '977' => 'Nepal (977)',
    '31' => 'Netherlands (31)',
    '599' => 'Netherlands Antilles (599)',
    '687' => 'New Caledonia (687)',
    '64' => 'New Zealand (64)',
    '505' => 'Nicaragua (505)',
    '227' => 'Niger (227)',
    '234' => 'Nigeria (234)',
    '850' => 'North Korea (850)',
    '47' => 'Norway (47)',
    '968' => 'Oman (968)',
    '92' => 'Pakistan (92)',
    '972' => 'Palestinian Authority (972)',
    '507' => 'Panama (507)',
    '675' => 'Papua New Guinea (675)',
    '595' => 'Paraguay (595)',
    '51' => 'Peru (51)',
    '63' => 'Philippines (63)',
    '48' => 'Poland (48)',
    '351' => 'Portugal (351)',
    '974' => 'Qatar (974)',
    '262' => 'Reunion (262)',
    '40' => 'Romania (40)',
    '7' => 'Russia (7)',
    '250' => 'Rwanda (250)',
    '378' => 'San Marino (378)',
    '239' => 'Sao Tome and Principe (239)',
    '881' => 'Satellite (881)',
    '966' => 'Saudi Arabia (966)',
    '221' => 'Senegal (221)',
    '381' => 'Serbia (381)',
    '248' => 'Seychelles (248)',
    '232' => 'Sierra Leone (232)',
    '65' => 'Singapore (65)',
    '421' => 'Slovakia (421)',
    '386' => 'Slovenia (386)',
    '252' => 'Somalia (252)',
    '27' => 'South Africa (27)',
    '82' => 'South Korea (82)',
    '34' => 'Spain (34)',
    '94' => 'Sri Lanka (94)',
    '1869' => 'St Kitts & Nevis (1869)',
    '1758' => 'St. Lucia (1758)',
    '1784' => 'St. Vincent & The Grenadines (1784)',
    '249' => 'Sudan (249)',
    '597' => 'Suriname (597)',
    '268' => 'Swaziland (268)',
    '46' => 'Sweden (46)',
    '41' => 'Switzerland (41)',
    '963' => 'Syria (963)',
    '886' => 'Taiwan (886)',
    '992' => 'Tajikistan (992)',
    '255' => 'Tanzania (255)',
    '66' => 'Thailand (66)',
    '228' => 'Togo (228)',
    '676' => 'Tonga (676)',
    '1868' => 'Trinidad and Tobago (1868)',
    '216' => 'Tunisia (216)',
    '90' => 'Turkey (90)',
    '993' => 'Turkmenistan (993)',
    '1649' => 'Turks & Caicos Islands (1649)',
    '256' => 'Uganda (256)',
    '380' => 'Ukraine (380)',
    '971' => 'United Arab Emirates (971)',
    '44' => 'United Kingdom (44)',
    '1' => 'United States (1)',
    '998' => 'Uzbekistan (998)',
    '678' => 'Vanuatu (678)',
    '58' => 'Venezuela (58)',
    '84' => 'Vietnam (84)',
    '1' => 'Virgin Islands (USA) (1)',
    '685' => 'Western Samoa (685)',
    '967' => 'Yemen (967)',
    '381' => 'Yugoslavia (381)',
    '260' => 'Zambia (260)',
    '263' => 'Zimbabwe (263)',
  );
}
